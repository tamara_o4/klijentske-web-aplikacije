import { Component, Input, OnInit } from '@angular/core';
import { Putnik } from '../models/putnik';
import { PutnikService } from '../services/putnik.service';
@Component({
  selector: 'app-putnici-izmena',
  templateUrl: './putnici-izmena.component.html',
  styleUrls: ['./putnici-izmena.component.css']
})
export class PutniciIzmenaComponent implements OnInit {

 
  @Input()
  putnik: Putnik = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public putnikService: PutnikService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.putnikService.update(this.putnik.id, {...this.putnik});
  }

}
