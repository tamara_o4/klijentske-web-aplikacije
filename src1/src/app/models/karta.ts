import { Stanica } from "./stanica";
import { Putnik } from "./putnik";
export interface Karta {
    id: number,
    datumVremePolaska: Date,
    prtljag:boolean,
    cena:number,
    putnik:Putnik|null,
    stanica:Stanica|null
}
