import { Component, OnInit } from '@angular/core';
import { Karta } from '../models/karta';
import { PutnikService } from '../services/putnik.service';
import { StanicaService } from '../services/stanica.service';
import { KartaService } from '../services/karta.service';
@Component({
  selector: 'app-karte-dodavanje',
  templateUrl: './karte-dodavanje.component.html',
  styleUrls: ['./karte-dodavanje.component.css']
})
export class KarteDodavanjeComponent implements OnInit {

  
  tiprtljaga: string[] = ["True", "False"];
  karta: Karta = {
    id: 1,
    datumVremePolaska: new Date(),
    prtljag:false,
    cena:0,
  
    putnik: null,
    stanica: null
  };

  constructor(public putnikService: PutnikService, public stanicaService: StanicaService, private kartaService: KartaService) { }

  ngOnInit(): void {
  }

  dodaj() {
    this.kartaService.create({...this.karta});
    console.log(this.karta);
  }

}
