import { Injectable } from '@angular/core';
import { Stanica } from '../models/stanica';
@Injectable({
  providedIn: 'root'
})
export class StanicaService {
 
  private stanice: Stanica[] = [
    {id: 1, naziv: "dasda", adresa: "kkkkkkkkkkk"},
    {id: 2, naziv: "nghfg", adresa: "kkkkkfasdfaskkkkkk"},
  ];
  constructor() { }

  getAll() {
    return this.stanice;
  }

  getOne(id: number) {
    for(let k of this.stanice) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(racun: Stanica) {
    this.stanice.push(racun);
  }

  update(id: number, racun: Stanica) {
    for(let i = 0; i < this.stanice.length; i++) {
      if(this.stanice[i].id == id) {
        this.stanice[i] = racun;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.stanice.length; i++) {
      if(this.stanice[i].id == id) {
        this.stanice.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
