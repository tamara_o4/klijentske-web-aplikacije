import { Injectable } from '@angular/core';
import { Putnik } from '../models/putnik';

@Injectable({
  providedIn: 'root'
})
export class PutnikService {
 

  private putnici: Putnik[] = [
    {id: 1, ime: "Sima", prezime: "Simic"},
    {id: 2, ime: "Petar", prezime: "Petrovic"},
  ];
  constructor() { }

  getAll() {
    return this.putnici;
  }

  getOne(id: number) {
    for(let k of this.putnici) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(putnik: Putnik) {
    if(this.getOne(putnik.id) == null){
      this.putnici.push(putnik);
      return true;
    }
    else{
      return false;
    }
  }

  update(id: number, putnik: Putnik) {
    for(let i = 0; i < this.putnici.length; i++) {
      if(this.putnici[i].id == id) {
        this.putnici[i] = putnik;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.putnici.length; i++) {
      if(this.putnici[i].id == id) {
        this.putnici.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
