import { Injectable } from '@angular/core';
import { Karta } from '../models/karta';
import { PutnikService } from './putnik.service';
import { StanicaService } from './stanica.service';
@Injectable({
  providedIn: 'root'
})
export class KartaService {
  private karte: Karta[] = [];

  constructor(private putnikService: PutnikService, private stanicaService: StanicaService) {
    this.karte.push({
      id: 1,
      datumVremePolaska: new Date(),
      prtljag:false,
      cena:0,
      putnik:putnikService.getOne(1),
      stanica:stanicaService.getOne(2)
      
    })
  }

  getAll() {
    return this.karte;
  }

  getOne(id: number) {
    for(let k of this.karte) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  // if(this.getOne(klijent.id) == null){
  //   this.klijenti.push(klijent);
  //   return true;
  // }
  // else{
  //   return false;
  // }

  create(transakcija: Karta) {
    // if(this.getOne(transakcija.datumTransakcije) > new Date())
    // {
      this.karte.push(transakcija);
    // }
    // else{
    //   console.log("Prosao je datum.");
    // }
  }

  update(id: number, karta: Karta) {
    for(let i = 0; i < this.karte.length; i++) {
      if(this.karte[i].id == id) {
        this.karte[i] = karta;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.karte.length; i++) {
      if(this.karte[i].id == id) {
        this.karte.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
