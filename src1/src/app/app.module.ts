import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { KlijentiComponent } from './klijenti/klijenti.component';
import { KlijentiDodavanjeComponent } from './klijenti-dodavanje/klijenti-dodavanje.component';
import { KlijentiIzmenaComponent } from './klijenti-izmena/klijenti-izmena.component';
import { RacuniComponent } from './racuni/racuni.component';
import { RacuniDodavanjeComponent } from './racuni-dodavanje/racuni-dodavanje.component';
import { RacuniIzmenaComponent } from './racuni-izmena/racuni-izmena.component';
import { TransakcijeComponent } from './transakcije/transakcije.component';
import { TransakcijeDodavanjeComponent } from './transakcije-dodavanje/transakcije-dodavanje.component';
import { TransakcijeIzmenaComponent } from './transakcije-izmena/transakcije-izmena.component';
import { KarteComponent } from './karte/karte.component';
import { KarteDodavanjeComponent } from './karte-dodavanje/karte-dodavanje.component';
import { KarteIzmenaComponent } from './karte-izmena/karte-izmena.component';
import { PutniciComponent } from './putnici/putnici.component';
import { PutniciDodavanjeComponent } from './putnici-dodavanje/putnici-dodavanje.component';
import { PutniciIzmenaComponent } from './putnici-izmena/putnici-izmena.component';
import { StanicaComponent } from './stanica/stanica.component';
import { StanicaDodavanjeComponent } from './stanica-dodavanje/stanica-dodavanje.component';
import { StanicaIzmenaComponent } from './stanica-izmena/stanica-izmena.component';

@NgModule({
  declarations: [
    AppComponent,
    KlijentiComponent,
    KlijentiDodavanjeComponent,
    KlijentiIzmenaComponent,
    RacuniComponent,
    RacuniDodavanjeComponent,
    RacuniIzmenaComponent,
    TransakcijeComponent,
    TransakcijeDodavanjeComponent,
    TransakcijeIzmenaComponent,
    KarteComponent,
    KarteDodavanjeComponent,
    KarteIzmenaComponent,
    PutniciComponent,
    PutniciDodavanjeComponent,
    PutniciIzmenaComponent,
    StanicaComponent,
    StanicaDodavanjeComponent,
    StanicaIzmenaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
