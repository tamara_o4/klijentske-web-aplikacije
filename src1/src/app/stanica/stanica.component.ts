import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Stanica } from '../models/stanica';
import { StanicaService } from '../services/stanica.service';

@Component({
  selector: 'app-stanica',
  templateUrl: './stanica.component.html',
  styleUrls: ['./stanica.component.css']
})
export class StanicaComponent implements OnInit {

  
  @Output()
  stanicaIzmena: EventEmitter<Stanica> = new EventEmitter<Stanica>();
 

  constructor(public stanicaService: StanicaService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.stanicaService.delete(id);
  }

  izmeni(stanica: Stanica){
    this.stanicaIzmena.emit({...stanica});
    console.log(stanica);
  }


}
