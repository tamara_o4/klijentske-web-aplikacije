import { Component, OnInit } from '@angular/core';
import { Stanica } from '../models/stanica';
import { StanicaService } from '../services/stanica.service';

@Component({
  selector: 'app-stanica-dodavanje',
  templateUrl: './stanica-dodavanje.component.html',
  styleUrls: ['./stanica-dodavanje.component.css']
})
export class StanicaDodavanjeComponent implements OnInit {

 
  stanica: Stanica = {
    id: 0,
    naziv:"",
    adresa: ""
  }

  constructor(public stanicaService: StanicaService) { }

  ngOnInit(): void {
  }

  dodaj(){
    this.stanicaService.create({...this.stanica});
  }
}
