import { Component, Input, OnInit } from '@angular/core';
import { Stanica } from '../models/stanica';
import { StanicaService } from '../services/stanica.service';

@Component({
  selector: 'app-stanica-izmena',
  templateUrl: './stanica-izmena.component.html',
  styleUrls: ['./stanica-izmena.component.css']
})
export class StanicaIzmenaComponent implements OnInit {
  @Input()
  stanica: Stanica = {
    id: 0,
    naziv:"",
    adresa:""   
  }

  constructor(public stanicaService: StanicaService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.stanicaService.update(this.stanica.id, {...this.stanica});
  }

}
