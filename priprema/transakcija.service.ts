import { EventEmitter, Injectable, Output } from '@angular/core';
import { Transakcija } from '../model/transakcija';
import { KlijentService } from './klijent.service';
import { RacunService } from './racun.service';

@Injectable({
  providedIn: 'root'
})
export class TransakcijaService {
  // prvo pravis niz
  private transakcije: Transakcija[] = [];

  constructor(private klijentServis: KlijentService, private racunServis: RacunService) {
    this.transakcije.push({
      id: 1,
      tip: "uplata",
      iznos: 5000,
      datumTransakcije: new Date(),
      klijent: klijentServis.getOne(1),
      racun: racunServis.getOne(2)
    })
  }

  getAll() {
    return this.transakcije;
  }

  getOne(id: number) {
    for(let k of this.transakcije) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(transakcija: Transakcija) {
    this.transakcije.push(transakcija);
  }

  update(id: number, transakcija: Transakcija) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije[i] = transakcija;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
