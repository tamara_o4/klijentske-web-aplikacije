import { Korisnik } from "./korisnik";
import { Proizvod } from "./proizvod";

export interface Kupovina {
    id: number,
    korisnik: Korisnik,
    proizvod: Proizvod,
    kolicina: number
}