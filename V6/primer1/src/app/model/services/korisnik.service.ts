import { Injectable } from '@angular/core';
import { Korisnik } from '../model/korisnik';

@Injectable({
  providedIn: 'root'
})
export class KorisnikService {
  korisnici: Korisnik[] = [
    { id: 1, korisnickoIme: "Korisnik1", email: "korisnik1@korisnik", lozinka: "" },
    { id: 2, korisnickoIme: "Korisnik2", email: "korisnik2@korisnik", lozinka: "" }
  ];

  constructor() { }

  getAll() {
    return this.korisnici;
  }

  getById(id: number) {
    for (let k of this.korisnici) {
      if (k.id == id) {
        return k;
      }
    }
    return null;
  }

  insert(korisnik: Korisnik) {
    this.korisnici.push(korisnik);
  }

  update(id: number, korisnik: Korisnik) {
    for (let i = 0; i < this.korisnici.length; i++) {
      if (this.korisnici[i].id == id) {
        this.korisnici[i] = korisnik;
      }
    }
  }
   
   
  // brisanje
  delete(id: number) {
    let index = null;
    for (let i = 0; i < this.korisnici.length; i++) {
      if (this.korisnici[i].id == id) {
        index = i;
      }
    }
    
    if (index != null) {
      this.korisnici.splice(index, 1);
    }
  }
}
