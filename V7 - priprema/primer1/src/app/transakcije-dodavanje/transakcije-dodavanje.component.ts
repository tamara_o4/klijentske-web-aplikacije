import { Component, OnInit } from '@angular/core';
import { Transakcija } from '../model/transakcija';
import { KlijentService } from '../service/klijent.service';
import { RacunService } from '../service/racun.service';
import { TransakcijaService } from '../service/transakcija.service';

@Component({
  selector: 'app-transakcije-dodavanje',
  templateUrl: './transakcije-dodavanje.component.html',
  styleUrls: ['./transakcije-dodavanje.component.css']
})
export class TransakcijeDodavanjeComponent implements OnInit {
  tipoviTransakcije: string[] = ["Uplata", "Isplata"];
  transakcija: Transakcija = {
    id: 1,
    iznos: 0,
    tip: "Uplata",
    datumTransakcije: new Date(),
    klijent: null,
    racun: null
  };
  constructor(public klijentServis: KlijentService, public racunServis: RacunService, private transkacijaServis: TransakcijaService) { }

  ngOnInit(): void {
  }

  dodaj() {
    this.transkacijaServis.create({...this.transakcija});
    console.log(this.transakcija);
  }

}
