import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransakcijeIzmenaComponent } from './transakcije-izmena.component';

describe('TransakcijeIzmenaComponent', () => {
  let component: TransakcijeIzmenaComponent;
  let fixture: ComponentFixture<TransakcijeIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransakcijeIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransakcijeIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
