import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TransakcijeComponent } from './transakcije/transakcije.component';
import { TransakcijeDodavanjeComponent } from './transakcije-dodavanje/transakcije-dodavanje.component';
import { TransakcijeIzmenaComponent } from './transakcije-izmena/transakcije-izmena.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TransakcijeComponent,
    TransakcijeDodavanjeComponent,
    TransakcijeIzmenaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
