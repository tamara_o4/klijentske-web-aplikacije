import { Klijent } from './../model/klijent';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KlijentService {
// prvo pravis niz
private klijenti: Klijent[] = [
  {id: 1, ime: "Pera", prezime: "Perić"},
  {id: 2, ime: "Marko", prezime: "Marković"},];

constructor() {
}

getAll() {
  return this.klijenti;
}

getOne(id: number) {
  for(let k of this.klijenti) {
    if(k.id == id) {
      return k;
    }
  }
  return null;
}

create(Klijent: Klijent) {
  this.klijenti.push(Klijent);
}

update(id: number, Klijent: Klijent) {
  for(let i = 0; i < this.klijenti.length; i++) {
    if(this.klijenti[i].id == id) {
      this.klijenti[i] = Klijent;
      return true;
    }
  }
  return false;
}

delete(id: number) {
  for(let i = 0; i < this.klijenti.length; i++) {
    if(this.klijenti[i].id == id) {
      this.klijenti.splice(i, 1);
      return true;
    }
  }
  return false;
}
}
