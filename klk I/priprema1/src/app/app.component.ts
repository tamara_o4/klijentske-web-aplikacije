import { Kupovina } from './model/kupovina';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'priprema1';

  kupovinaIzmena: Kupovina|null = null;

  izmeni4(kupovina: Kupovina) {
    this.kupovinaIzmena = kupovina;
  }

}
