import { Proizvod } from './proizvod';
import { Kupac } from './kupac';

export interface Kupovina {
    id: number,
    datumKupovine: Date,
    kolicina: number,
    kupac: Kupac|null,
    proizvod: Proizvod|null
}
