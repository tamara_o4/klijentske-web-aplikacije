import { Proizvod } from './../model/proizvod';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ProizvodService {
  // prvo pravis niz
  private proizvod: Proizvod[] = [
    {id: 1, naziv: "aaa", opis: "Perić", cena: 520},
    {id: 2, naziv: "sss", opis: "Marković", cena: 850}];

  constructor() {

  }

  getAll() {
    return this.proizvod;
  }

  getOne(id: number) {
    for(let k of this.proizvod) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(Proizvod: Proizvod) {
    this.proizvod.push(Proizvod);
  }

  update(id: number, Proizvod: Proizvod) {
    for(let i = 0; i < this.proizvod.length; i++) {
      if(this.proizvod[i].id == id) {
        this.proizvod[i] = Proizvod;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.proizvod.length; i++) {
      if(this.proizvod[i].id == id) {
        this.proizvod.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
