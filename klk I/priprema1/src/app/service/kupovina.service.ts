import { ProizvodService } from './proizvod.service';
import { KupacService } from './kupac.service';
import { Kupovina } from './../model/kupovina';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class KupovinaService {

  private transakcije: Kupovina[] = [];

  constructor(private klijentServis: KupacService, private racunServis: ProizvodService) {
    this.transakcije.push({
      id: 1,
      datumKupovine: new Date(),
      kolicina: 5000,
      kupac: klijentServis.getOne(1),
      proizvod: racunServis.getOne(2)
    })
  }

  getAll() {
    return this.transakcije;
  }

  getOne(id: number) {
    for(let k of this.transakcije) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  // if(this.getOne(klijent.id) == null){
  //   this.klijenti.push(klijent);
  //   return true;
  // }
  // else{
  //   return false;
  // }

  create(Kupovina: Kupovina) {
    // if(this.getOne(Kupovina.datumTransakcije) > new Date())
    // {
      this.transakcije.push(Kupovina);
    // }
    // else{
    //   console.log("Prosao je datum.");
    // }
  }

  update(id: number, Kupovina: Kupovina) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije[i] = Kupovina;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}