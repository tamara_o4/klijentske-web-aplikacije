import { Kupac } from './../model/kupac';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class KupacService {
  // prvo pravis niz
  private kupac: Kupac[] =[
    {id: 1, ime: "Pera", prezime: "Perić", email: "a@gmail.com"},
    {id: 2, ime: "Marko", prezime: "Marković", email: "b@gmail.com"},];

  constructor() {

  }

  getAll() {
    return this.kupac;
  }

  getOne(id: number) {
    for(let k of this.kupac) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(Kupac: Kupac) {
    this.kupac.push(Kupac);
  }

  update(id: number, Kupac: Kupac) {
    for(let i = 0; i < this.kupac.length; i++) {
      if(this.kupac[i].id == id) {
        this.kupac[i] = Kupac;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.kupac.length; i++) {
      if(this.kupac[i].id == id) {
        this.kupac.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
