
import { Kupac } from './../model/kupac';
import { Kupovina } from './../model/kupovina';
import { KupovinaService } from './../service/kupovina.service';
import { Proizvod } from './../model/proizvod';
import { ProizvodService } from './../service/proizvod.service';
import { KupacService } from './../service/kupac.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kupovina-dodavanje',
  templateUrl: './kupovina-dodavanje.component.html',
  styleUrls: ['./kupovina-dodavanje.component.css']
})
export class KupovinaDodavanjeComponent implements OnInit {


  kupovina: Kupovina = {
    id: 0,
    datumKupovine: new Date(),
    kolicina: 0,
    kupac:  null,
    proizvod:  null
  }

  constructor(public kupovinaServis: KupovinaService, public kupacService: KupacService, public proizvodService: ProizvodService) { }

  ngOnInit(): void {
  }

  dodaj(){
    this.kupovinaServis.create({...this.kupovina});
  }
}
