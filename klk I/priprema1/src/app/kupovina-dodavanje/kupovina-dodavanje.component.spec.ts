import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KupovinaDodavanjeComponent } from './kupovina-dodavanje.component';

describe('KupovinaDodavanjeComponent', () => {
  let component: KupovinaDodavanjeComponent;
  let fixture: ComponentFixture<KupovinaDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KupovinaDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KupovinaDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
