import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';  

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KupovineComponent } from './kupovine/kupovine.component';
import { KupciComponent } from './kupci/kupci.component';
import { ProizvodiComponent } from './proizvodi/proizvodi.component';
import { KupovinaDodavanjeComponent } from './kupovina-dodavanje/kupovina-dodavanje.component';
import { KupovinaIzmenaComponent } from './kupovina-izmena/kupovina-izmena.component';

@NgModule({
  declarations: [
    AppComponent,
    KupovineComponent,
    KupciComponent,
    ProizvodiComponent,
    KupovinaDodavanjeComponent,
    KupovinaIzmenaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
