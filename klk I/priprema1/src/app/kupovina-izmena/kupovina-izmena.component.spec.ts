import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KupovinaIzmenaComponent } from './kupovina-izmena.component';

describe('KupovinaIzmenaComponent', () => {
  let component: KupovinaIzmenaComponent;
  let fixture: ComponentFixture<KupovinaIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KupovinaIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KupovinaIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
