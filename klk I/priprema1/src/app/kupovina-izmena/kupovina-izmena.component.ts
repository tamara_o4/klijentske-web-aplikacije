import { ProizvodService } from './../service/proizvod.service';
import { KupacService } from './../service/kupac.service';
import { KupovinaService } from './../service/kupovina.service';
import { Kupovina } from './../model/kupovina';
import { Component, OnInit, Input , Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-kupovina-izmena',
  templateUrl: './kupovina-izmena.component.html',
  styleUrls: ['./kupovina-izmena.component.css']
})
export class KupovinaIzmenaComponent implements OnInit {
 
  @Input()
  kupovina: Kupovina = {
    id: 0,
    datumKupovine: new Date(),
    kolicina: 0,
    kupac:  null,
    proizvod:  null
  }

  constructor(public kupovinaServis: KupovinaService, public kupacService: KupacService, public proizvodService: ProizvodService) { }

  ngOnInit(): void {
  }
  izmeni() {
    this.kupovinaServis.update(this.kupovina.id, {...this.kupovina});
  }

  ukloni(id: number) {
    this.kupovinaServis.delete(id);
  }

}
