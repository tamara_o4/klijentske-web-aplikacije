import { KupovinaService } from './../service/kupovina.service';
import { Kupovina } from './../model/kupovina';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-kupovine',
  templateUrl: './kupovine.component.html',
  styleUrls: ['./kupovine.component.css']
})
export class KupovineComponent implements OnInit {

  @Output()
  kupovinaIzmena: EventEmitter<Kupovina> = new EventEmitter<Kupovina>();

  constructor(public kupovineServis: KupovinaService) { }

  ngOnInit(): void {
  }

  izmeni(karta: Kupovina) {
    this.kupovinaIzmena.emit({...karta});
  }
}
