import { Injectable } from '@angular/core';
import { Korisnik } from '../model/korisnik';

@Injectable({
  providedIn: 'root'
})
export class KorisnikService {
 

  private korisnici: Korisnik[] = [
    {id: 1, ime: "Sima", prezime: "Simic"},
    {id: 2, ime: "Petar", prezime: "Petrovic"},
  ];
  constructor() { }

  getAll() {
    return this.korisnici;
  }

  getOne(id: number) {
    for(let k of this.korisnici) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(korisnik: Korisnik) {
    if(this.getOne(korisnik.id) == null){
      this.korisnici.push(korisnik);
      return true;
    }
    else{
      return false;
    }
  }

  update(id: number, korisnik: Korisnik) {
    for(let i = 0; i < this.korisnici.length; i++) {
      if(this.korisnici[i].id == id) {
        this.korisnici[i] = korisnik;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.korisnici.length; i++) {
      if(this.korisnici[i].id == id) {
        this.korisnici.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
