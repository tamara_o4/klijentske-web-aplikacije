import { Injectable } from '@angular/core';
import { Iznajmljivanje } from '../model/iznajmljivanje';
import { KnjigaService } from './knjiga.service';
import { KorisnikService } from './korisnik.service';

@Injectable({
  providedIn: 'root'
})
export class IznajmljivanjeService {
  private iznajmljivanja: Iznajmljivanje[] = [];

  constructor(private korisnikService: KorisnikService, private knjigaService: KnjigaService) {
    this.iznajmljivanja.push({
      id: 1,
      datumPreuzimanja: new Date(),
      datumVracanja: new Date(),
      stanje:"",
      cena:0,
      korisnik:korisnikService.getOne(1),
      knjiga:knjigaService.getOne(2)
      
    })
  }

  getAll() {
    return this.iznajmljivanja;
  }

  getOne(id: number) {
    for(let k of this.iznajmljivanja) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  // if(this.getOne(klijent.id) == null){
  //   this.klijenti.push(klijent);
  //   return true;
  // }
  // else{
  //   return false;
  // }

  create(iznajmljivanje: Iznajmljivanje) {
    // if(this.getOne(iznajmljivanje.datumTransakcije) > new Date())
    // {
      this.iznajmljivanja.push(iznajmljivanje);
    // }
    // else{
    //   console.log("Prosao je datum.");
    // }
  }

  update(id: number, karta: Iznajmljivanje) {
    for(let i = 0; i < this.iznajmljivanja.length; i++) {
      if(this.iznajmljivanja[i].id == id) {
        this.iznajmljivanja[i] = karta;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.iznajmljivanja.length; i++) {
      if(this.iznajmljivanja[i].id == id) {
        this.iznajmljivanja.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
