import { Injectable } from '@angular/core';
import { Knjiga } from '../model/knjiga';

@Injectable({
  providedIn: 'root'
})
export class KnjigaService {
 

  private knjige: Knjiga[] = [
    {id: 1, naslov: "prva", autor: "prvi", zanr : "neki"},
    {id: 2, naslov: "knjiga", autor: "autor2", zanr : "drama"},
  ];
  constructor() { }

  getAll() {
    return this.knjige;
  }

  getOne(id: number) {
    for(let k of this.knjige) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(knjiga: Knjiga) {
    if(this.getOne(knjiga.id) == null){
      this.knjige.push(knjiga);
      return true;
    }
    else{
      return false;
    }
  }

  update(id: number, knjiga: Knjiga) {
    for(let i = 0; i < this.knjige.length; i++) {
      if(this.knjige[i].id == id) {
        this.knjige[i] = knjiga;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.knjige.length; i++) {
      if(this.knjige[i].id == id) {
        this.knjige.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
