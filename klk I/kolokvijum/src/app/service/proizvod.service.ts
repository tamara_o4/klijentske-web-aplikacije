import { Injectable } from '@angular/core';
import { Proizvod } from '../model/proizvod';

@Injectable({
  providedIn: 'root'
})
export class ProizvodService {


  private proizvodi: Proizvod[] = [
    {id: 1, naziv: "dada", opis: "Sdadadimic", cena: 552},
    {id: 1, naziv: "dadaffew", opis: "ggre", cena: 232},
  ];
  constructor() { }

  getAll() {
    return this.proizvodi;
  }

  getOne(id: number) {
    for(let k of this.proizvodi) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(Proizvod: Proizvod) {
    if(this.getOne(Proizvod.id) == null){
      this.proizvodi.push(Proizvod);
      return true;
    }
    else{
      return false;
    }
  }

  update(id: number, Proizvod: Proizvod) {
    for(let i = 0; i < this.proizvodi.length; i++) {
      if(this.proizvodi[i].id == id) {
        this.proizvodi[i] = Proizvod;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.proizvodi.length; i++) {
      if(this.proizvodi[i].id == id) {
        this.proizvodi.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
