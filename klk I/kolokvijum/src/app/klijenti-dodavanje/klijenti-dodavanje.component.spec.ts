import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KlijentiDodavanjeComponent } from './klijenti-dodavanje.component';

describe('KlijentiDodavanjeComponent', () => {
  let component: KlijentiDodavanjeComponent;
  let fixture: ComponentFixture<KlijentiDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KlijentiDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KlijentiDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
