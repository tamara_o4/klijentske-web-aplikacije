import { Component, OnInit } from '@angular/core';
import { Klijent } from '../models/klijent';
import { KlijentService } from '../services/klijent.service';

@Component({
  selector: 'app-klijenti-dodavanje',
  templateUrl: './klijenti-dodavanje.component.html',
  styleUrls: ['./klijenti-dodavanje.component.css']
})
export class KlijentiDodavanjeComponent implements OnInit {

  klijent: Klijent = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public klijentiServis: KlijentService) { }

  ngOnInit(): void {
  }

  dodaj(){
    if(this.klijentiServis.create({...this.klijent}) == true)
    {
      this.klijentiServis.create({...this.klijent});
    }
    else{
      console.log("Korisnik sa tim ID postoji.");
    }
  }

}
