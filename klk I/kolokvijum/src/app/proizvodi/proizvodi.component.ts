import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Proizvod } from '../model/proizvod';
import { ProizvodService } from '../service/proizvod.service';

@Component({
  selector: 'app-proizvodi',
  templateUrl: './proizvodi.component.html',
  styleUrls: ['./proizvodi.component.css']
})
export class ProizvodiComponent implements OnInit {
  @Output()
  proizvodIzmena: EventEmitter<Proizvod> = new EventEmitter<Proizvod>();
  

  constructor(public proizvodService: ProizvodService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.proizvodService.delete(id);
  }

  izmeni(proizvod: Proizvod){
    this.proizvodIzmena.emit({...proizvod});
    console.log(proizvod);
  }

}
