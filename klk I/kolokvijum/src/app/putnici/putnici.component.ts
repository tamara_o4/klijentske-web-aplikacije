import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Putnik } from '../models/putnik';
import { PutnikService } from '../services/putnik.service';
@Component({
  selector: 'app-putnici',
  templateUrl: './putnici.component.html',
  styleUrls: ['./putnici.component.css']
})
export class PutniciComponent implements OnInit {
  @Output()
  putnikIzmena: EventEmitter<Putnik> = new EventEmitter<Putnik>();
  

  constructor(public putnikService: PutnikService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.putnikService.delete(id);
  }

  izmeni(putnik: Putnik){
    this.putnikIzmena.emit({...putnik});
    console.log(putnik);
  }
}
