import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StanicaIzmenaComponent } from './stanica-izmena.component';

describe('StanicaIzmenaComponent', () => {
  let component: StanicaIzmenaComponent;
  let fixture: ComponentFixture<StanicaIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StanicaIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StanicaIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
