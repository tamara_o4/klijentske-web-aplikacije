import { Component, OnInit } from '@angular/core';
import { Kupac } from '../model/kupac';
import { KupacService } from '../service/kupac.service';

@Component({
  selector: 'app-kupci-dodavanje',
  templateUrl: './kupci-dodavanje.component.html',
  styleUrls: ['./kupci-dodavanje.component.css']
})
export class KupciDodavanjeComponent implements OnInit {

  kupac: Kupac = {
    id: 0,
    ime: "",
    prezime: "",
    email: ""
  }

  constructor(public kupacService: KupacService) { }

  ngOnInit(): void {
  }

  // dodaj(){
  //   if(this.kupacService.create({...this.kupac}) == true)
  //   {
  //     this.kupacService.create({...this.kupac});
  //   }
  //   else{
  //     console.log("Korisnik sa tim ID postoji.");
  //   }
  // }

  dodaj(){
    this.kupacService.create({...this.kupac});
  }

}
