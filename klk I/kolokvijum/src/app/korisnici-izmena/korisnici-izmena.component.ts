import { Component, Input, OnInit } from '@angular/core';
import { Korisnik } from '../model/korisnik';
import { KorisnikService } from '../service/korisnik.service';

@Component({
  selector: 'app-korisnici-izmena',
  templateUrl: './korisnici-izmena.component.html',
  styleUrls: ['./korisnici-izmena.component.css']
})
export class KorisniciIzmenaComponent implements OnInit {

 
  @Input()
  korisnik: Korisnik = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public korisnikService: KorisnikService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.korisnikService.update(this.korisnik.id, {...this.korisnik});
  }

}
