import { Component, OnInit } from '@angular/core';

import { Putnik } from '../models/putnik';
import { PutnikService } from '../services/putnik.service';
@Component({
  selector: 'app-putnici-dodavanje',
  templateUrl: './putnici-dodavanje.component.html',
  styleUrls: ['./putnici-dodavanje.component.css']
})
export class PutniciDodavanjeComponent implements OnInit {

  putnik: Putnik = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public putnikService: PutnikService) { }

  ngOnInit(): void {
  }

  dodaj(){
    if(this.putnikService.create({...this.putnik}) == true)
    {
      this.putnikService.create({...this.putnik});
    }
    else{
      console.log("Korisnik sa tim ID postoji.");
    }
  }
}
