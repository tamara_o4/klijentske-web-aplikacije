import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { KlijentiComponent } from './klijenti/klijenti.component';
import { KlijentiDodavanjeComponent } from './klijenti-dodavanje/klijenti-dodavanje.component';
import { KlijentiIzmenaComponent } from './klijenti-izmena/klijenti-izmena.component';
import { RacuniComponent } from './racuni/racuni.component';
import { RacuniDodavanjeComponent } from './racuni-dodavanje/racuni-dodavanje.component';
import { RacuniIzmenaComponent } from './racuni-izmena/racuni-izmena.component';
import { TransakcijeComponent } from './transakcije/transakcije.component';
import { TransakcijeDodavanjeComponent } from './transakcije-dodavanje/transakcije-dodavanje.component';
import { TransakcijeIzmenaComponent } from './transakcije-izmena/transakcije-izmena.component';
import { KarteComponent } from './karte/karte.component';
import { KarteDodavanjeComponent } from './karte-dodavanje/karte-dodavanje.component';
import { KarteIzmenaComponent } from './karte-izmena/karte-izmena.component';
import { PutniciComponent } from './putnici/putnici.component';
import { PutniciDodavanjeComponent } from './putnici-dodavanje/putnici-dodavanje.component';
import { PutniciIzmenaComponent } from './putnici-izmena/putnici-izmena.component';
import { StanicaComponent } from './stanica/stanica.component';
import { StanicaDodavanjeComponent } from './stanica-dodavanje/stanica-dodavanje.component';
import { StanicaIzmenaComponent } from './stanica-izmena/stanica-izmena.component';
import { KupciComponent } from './kupci/kupci.component';
import { KupovineComponent } from './kupovine/kupovine.component';
import { ProizvodiComponent } from './proizvodi/proizvodi.component';
import { KupciDodavanjeComponent } from './kupci-dodavanje/kupci-dodavanje.component';
import { KupciIzmenaComponent } from './kupci-izmena/kupci-izmena.component';
import { KupovineDodavanjeComponent } from './kupovine-dodavanje/kupovine-dodavanje.component';
import { KupovineIzmenaComponent } from './kupovine-izmena/kupovine-izmena.component';
import { ProizvodiDodavanjeComponent } from './proizvodi-dodavanje/proizvodi-dodavanje.component';
import { ProizvodiIzmenaComponent } from './proizvodi-izmena/proizvodi-izmena.component';
import { KorisniciComponent } from './korisnici/korisnici.component';
import { KnjigeComponent } from './knjige/knjige.component';
import { IznajmljivanjaComponent } from './iznajmljivanja/iznajmljivanja.component';
import { IznajmljivanjaDidavanjeComponent } from './iznajmljivanja-didavanje/iznajmljivanja-didavanje.component';
import { IznajmljivanjaIzmenaComponent } from './iznajmljivanja-izmena/iznajmljivanja-izmena.component';
import { KnjigeIzmenaComponent } from './knjige-izmena/knjige-izmena.component';
import { KnjigeDodavanjeComponent } from './knjige-dodavanje/knjige-dodavanje.component';
import { KorisniciIzmenaComponent } from './korisnici-izmena/korisnici-izmena.component';
import { KorisniciDodavanjeComponent } from './korisnici-dodavanje/korisnici-dodavanje.component';

@NgModule({
  declarations: [
    AppComponent,
    KlijentiComponent,
    KlijentiDodavanjeComponent,
    KlijentiIzmenaComponent,
    RacuniComponent,
    RacuniDodavanjeComponent,
    RacuniIzmenaComponent,
    TransakcijeComponent,
    TransakcijeDodavanjeComponent,
    TransakcijeIzmenaComponent,
    KarteComponent,
    KarteDodavanjeComponent,
    KarteIzmenaComponent,
    PutniciComponent,
    PutniciDodavanjeComponent,
    PutniciIzmenaComponent,
    StanicaComponent,
    StanicaDodavanjeComponent,
    StanicaIzmenaComponent,
    KupciComponent,
    KupovineComponent,
    ProizvodiComponent,
    KupciDodavanjeComponent,
    KupciIzmenaComponent,
    KupovineDodavanjeComponent,
    KupovineIzmenaComponent,
    ProizvodiDodavanjeComponent,
    ProizvodiIzmenaComponent,
    KorisniciComponent,
    KnjigeComponent,
    IznajmljivanjaComponent,
    IznajmljivanjaDidavanjeComponent,
    IznajmljivanjaIzmenaComponent,
    KnjigeIzmenaComponent,
    KnjigeDodavanjeComponent,
    KorisniciIzmenaComponent,
    KorisniciDodavanjeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
