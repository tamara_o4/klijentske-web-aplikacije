import { Component, OnInit } from '@angular/core';
import { Knjiga } from '../model/knjiga';
import { KnjigaService } from '../service/knjiga.service';

@Component({
  selector: 'app-knjige-dodavanje',
  templateUrl: './knjige-dodavanje.component.html',
  styleUrls: ['./knjige-dodavanje.component.css']
})
export class KnjigeDodavanjeComponent implements OnInit {


  knjiga: Knjiga = {
    id: 0,
    naslov: "",
    autor: "",
    zanr: ""
  }

  constructor(public knjigaService: KnjigaService) { }

  ngOnInit(): void {
  }

  dodaj(){
    if(this.knjigaService.create({...this.knjiga}) == true)
    {
      this.knjigaService.create({...this.knjiga});
    }
    else{
      console.log("Korisnik sa tim ID postoji.");
    }
  }
}
