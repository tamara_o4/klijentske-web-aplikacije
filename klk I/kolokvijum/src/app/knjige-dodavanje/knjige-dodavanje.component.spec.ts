import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnjigeDodavanjeComponent } from './knjige-dodavanje.component';

describe('KnjigeDodavanjeComponent', () => {
  let component: KnjigeDodavanjeComponent;
  let fixture: ComponentFixture<KnjigeDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KnjigeDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KnjigeDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
