import { Component, Input, OnInit } from '@angular/core';
import { Racun } from '../models/racun';
import { RacunService } from '../services/racun.service';

@Component({
  selector: 'app-racuni-izmena',
  templateUrl: './racuni-izmena.component.html',
  styleUrls: ['./racuni-izmena.component.css']
})
export class RacuniIzmenaComponent implements OnInit {

  @Input()
  racun: Racun = {
    id: 0,
    brojRacuna: "",
    stanje: 0
  }

  constructor(public racuniServis: RacunService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.racuniServis.update(this.racun.id, {...this.racun});
  }

}
