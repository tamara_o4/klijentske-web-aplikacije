import { Component, Input, OnInit } from '@angular/core';
import { Iznajmljivanje } from '../model/iznajmljivanje';
import { IznajmljivanjeService } from '../service/iznajmljivanje.service';
import { KnjigaService } from '../service/knjiga.service';
import { KorisnikService } from '../service/korisnik.service';

@Component({
  selector: 'app-iznajmljivanja-izmena',
  templateUrl: './iznajmljivanja-izmena.component.html',
  styleUrls: ['./iznajmljivanja-izmena.component.css']
})
export class IznajmljivanjaIzmenaComponent implements OnInit {


  //tip: string[] = ["True", "False"];
  @Input()
  iznajmljivanje: Iznajmljivanje = {
    id: 1,
    datumPreuzimanja: new Date(),
    datumVracanja: new Date(),
    stanje:"",
    cena:0,
  
    korisnik: null,
    knjiga: null
  };


  constructor(public korisnikService: KorisnikService, public knjigaService: KnjigaService, private iznajmljivanjeService: IznajmljivanjeService) { }

  ngOnInit(): void {
  }
  izmeni() {
    this.iznajmljivanjeService.update(this.iznajmljivanje.id, {...this.iznajmljivanje});
  }
}
