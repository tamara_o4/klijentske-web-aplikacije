import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjaIzmenaComponent } from './iznajmljivanja-izmena.component';

describe('IznajmljivanjaIzmenaComponent', () => {
  let component: IznajmljivanjaIzmenaComponent;
  let fixture: ComponentFixture<IznajmljivanjaIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IznajmljivanjaIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjaIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
