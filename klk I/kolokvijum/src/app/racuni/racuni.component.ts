import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Racun } from '../models/racun';
import { RacunService } from '../services/racun.service';

@Component({
  selector: 'app-racuni',
  templateUrl: './racuni.component.html',
  styleUrls: ['./racuni.component.css']
})
export class RacuniComponent implements OnInit {

  @Output()
  racunIzmena: EventEmitter<Racun> = new EventEmitter<Racun>();

  constructor(public racuniServis: RacunService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.racuniServis.delete(id);
  }

  izmeni(racun: Racun){
    this.racunIzmena.emit({...racun});
    console.log(racun);
  }

}
