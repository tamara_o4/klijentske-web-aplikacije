import { Iznajmljivanje } from './../model/iznajmljivanje';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IznajmljivanjeService } from '../service/iznajmljivanje.service';

@Component({
  selector: 'app-iznajmljivanja',
  templateUrl: './iznajmljivanja.component.html',
  styleUrls: ['./iznajmljivanja.component.css']
})
export class IznajmljivanjaComponent implements OnInit {
  @Output()
  iznajmljivanjaIzmena: EventEmitter<Iznajmljivanje> = new EventEmitter<Iznajmljivanje>();
  constructor(public iznajmljivanjeService:IznajmljivanjeService) { }

  ngOnInit(): void {
  }
  ukloni(id: number) {
    this.iznajmljivanjeService.delete(id);
  }

  izmeni(iznajmljivanje: Iznajmljivanje) {
    this.iznajmljivanjaIzmena.emit({...iznajmljivanje});
  }


}
