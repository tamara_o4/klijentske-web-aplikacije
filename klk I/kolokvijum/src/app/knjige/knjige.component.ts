import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Knjiga } from '../model/knjiga';
import { KnjigaService } from '../service/knjiga.service';

@Component({
  selector: 'app-knjige',
  templateUrl: './knjige.component.html',
  styleUrls: ['./knjige.component.css']
})
export class KnjigeComponent implements OnInit {

  @Output()
  knjigaIzmena: EventEmitter<Knjiga> = new EventEmitter<Knjiga>();
  

  constructor(public knjigaService: KnjigaService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.knjigaService.delete(id);
  }

  izmeni(knjiga: Knjiga){
    this.knjigaIzmena.emit({...knjiga});
    console.log(knjiga);
  }

}
