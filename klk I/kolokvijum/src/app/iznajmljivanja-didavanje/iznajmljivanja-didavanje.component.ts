import { Component, OnInit } from '@angular/core';
import { Iznajmljivanje } from '../model/iznajmljivanje';
import { IznajmljivanjeService } from '../service/iznajmljivanje.service';
import { KnjigaService } from '../service/knjiga.service';
import { KorisnikService } from '../service/korisnik.service';

@Component({
  selector: 'app-iznajmljivanja-didavanje',
  templateUrl: './iznajmljivanja-didavanje.component.html',
  styleUrls: ['./iznajmljivanja-didavanje.component.css']
})
export class IznajmljivanjaDidavanjeComponent implements OnInit {

  
  //tip: string[] = ["True", "False"];
  iznajmljivanja: Iznajmljivanje = {
    id: 1,
    datumPreuzimanja: new Date(),
    datumVracanja: new Date(),
    stanje:"",
    cena:0,
  
    korisnik: null,
    knjiga: null
  };

  constructor(public korisnikService: KorisnikService, public knjigaService: KnjigaService, private iznajmljivanjaService: IznajmljivanjeService) { }

  ngOnInit(): void {
  }

  dodaj() {
    this.iznajmljivanjaService.create({...this.iznajmljivanja});
    console.log(this.iznajmljivanja);
  }
}
