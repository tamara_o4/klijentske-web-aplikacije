import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjaDidavanjeComponent } from './iznajmljivanja-didavanje.component';

describe('IznajmljivanjaDidavanjeComponent', () => {
  let component: IznajmljivanjaDidavanjeComponent;
  let fixture: ComponentFixture<IznajmljivanjaDidavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IznajmljivanjaDidavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjaDidavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
