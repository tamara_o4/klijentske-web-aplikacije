import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KupciIzmenaComponent } from './kupci-izmena.component';

describe('KupciIzmenaComponent', () => {
  let component: KupciIzmenaComponent;
  let fixture: ComponentFixture<KupciIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KupciIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KupciIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
