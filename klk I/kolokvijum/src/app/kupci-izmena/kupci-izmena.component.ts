import { Component, Input, OnInit } from '@angular/core';
import { Kupac } from '../model/kupac';
import { KupacService } from '../service/kupac.service';

@Component({
  selector: 'app-kupci-izmena',
  templateUrl: './kupci-izmena.component.html',
  styleUrls: ['./kupci-izmena.component.css']
})
export class KupciIzmenaComponent implements OnInit {

  
 
  @Input()
  kupac: Kupac = {
    id: 0,
    ime: "",
    prezime: "",
    email: ""
  }

  constructor(public kupacService: KupacService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.kupacService.update(this.kupac.id, {...this.kupac});
  }

}
