import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KlijentiIzmenaComponent } from './klijenti-izmena.component';

describe('KlijentiIzmenaComponent', () => {
  let component: KlijentiIzmenaComponent;
  let fixture: ComponentFixture<KlijentiIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KlijentiIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KlijentiIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
