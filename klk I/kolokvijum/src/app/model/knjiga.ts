export interface Knjiga {
    id: number,
    naslov: string,
    autor:string,
    zanr: string
}
