import { Knjiga } from './knjiga';
import { Korisnik } from './korisnik';
export interface Iznajmljivanje {
    id: number,
    datumPreuzimanja: Date,
    datumVracanja: Date,
    stanje:string,
    cena:number,
    korisnik:Korisnik|null,
    knjiga:Knjiga|null
}
