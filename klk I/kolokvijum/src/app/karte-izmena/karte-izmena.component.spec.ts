import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KarteIzmenaComponent } from './karte-izmena.component';

describe('KarteIzmenaComponent', () => {
  let component: KarteIzmenaComponent;
  let fixture: ComponentFixture<KarteIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KarteIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KarteIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
