import { Component, Input, OnInit } from '@angular/core';
import { Karta } from '../models/karta';
import { KartaService } from '../services/karta.service';
import { PutnikService } from '../services/putnik.service';
import { StanicaService } from '../services/stanica.service';

@Component({
  selector: 'app-karte-izmena',
  templateUrl: './karte-izmena.component.html',
  styleUrls: ['./karte-izmena.component.css']
})
export class KarteIzmenaComponent implements OnInit {

  tiprtljaga: string[] = ["True", "False"];
  @Input()
  karta: Karta = {
    id: 1,
    datumVremePolaska: new Date(),
    prtljag:false,
    cena:0,
    putnik: null,
    stanica: null
  };


  constructor(public putnikService: PutnikService, public stanicaService: StanicaService, private kartaService: KartaService) { }

  ngOnInit(): void {
  }
  izmeni() {
    this.kartaService.update(this.karta.id, {...this.karta});
  }
}
