import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProizvodiIzmenaComponent } from './proizvodi-izmena.component';

describe('ProizvodiIzmenaComponent', () => {
  let component: ProizvodiIzmenaComponent;
  let fixture: ComponentFixture<ProizvodiIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProizvodiIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProizvodiIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
