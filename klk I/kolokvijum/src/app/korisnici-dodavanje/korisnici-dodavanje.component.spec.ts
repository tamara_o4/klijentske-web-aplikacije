import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KorisniciDodavanjeComponent } from './korisnici-dodavanje.component';

describe('KorisniciDodavanjeComponent', () => {
  let component: KorisniciDodavanjeComponent;
  let fixture: ComponentFixture<KorisniciDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KorisniciDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KorisniciDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
