import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../model/korisnik';
import { KorisnikService } from '../service/korisnik.service';

@Component({
  selector: 'app-korisnici-dodavanje',
  templateUrl: './korisnici-dodavanje.component.html',
  styleUrls: ['./korisnici-dodavanje.component.css']
})
export class KorisniciDodavanjeComponent implements OnInit {

  korisnik: Korisnik = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public korisnikService: KorisnikService) { }

  ngOnInit(): void {
  }

  dodaj(){
    if(this.korisnikService.create({...this.korisnik}) == true)
    {
      this.korisnikService.create({...this.korisnik});
    }
    else{
      console.log("Korisnik sa tim ID postoji.");
    }
  }

}
