import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Korisnik } from '../model/korisnik';
import { KorisnikService } from '../service/korisnik.service';

@Component({
  selector: 'app-korisnici',
  templateUrl: './korisnici.component.html',
  styleUrls: ['./korisnici.component.css']
})
export class KorisniciComponent implements OnInit {
  @Output()
  korisnikIzmena: EventEmitter<Korisnik> = new EventEmitter<Korisnik>();
  

  constructor(public korisnikService: KorisnikService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.korisnikService.delete(id);
  }

  izmeni(korisnik: Korisnik){
    this.korisnikIzmena.emit({...korisnik});
    console.log(korisnik);
  }

}
