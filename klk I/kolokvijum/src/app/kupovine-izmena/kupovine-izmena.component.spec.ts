import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KupovineIzmenaComponent } from './kupovine-izmena.component';

describe('KupovineIzmenaComponent', () => {
  let component: KupovineIzmenaComponent;
  let fixture: ComponentFixture<KupovineIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KupovineIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KupovineIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
