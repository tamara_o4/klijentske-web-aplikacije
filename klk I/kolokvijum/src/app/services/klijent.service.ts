import { Injectable } from '@angular/core';
import { Klijent } from '../models/klijent';

@Injectable({
  providedIn: 'root'
})
export class KlijentService {

  private klijenti: Klijent[] = [
    {id: 1, ime: "Sima", prezime: "Simic"},
    {id: 2, ime: "Petar", prezime: "Petrovic"},
  ];
  constructor() { }

  getAll() {
    return this.klijenti;
  }

  getOne(id: number) {
    for(let k of this.klijenti) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(klijent: Klijent) {
    if(this.getOne(klijent.id) == null){
      this.klijenti.push(klijent);
      return true;
    }
    else{
      return false;
    }
  }

  update(id: number, klijent: Klijent) {
    for(let i = 0; i < this.klijenti.length; i++) {
      if(this.klijenti[i].id == id) {
        this.klijenti[i] = klijent;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.klijenti.length; i++) {
      if(this.klijenti[i].id == id) {
        this.klijenti.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
