import { Injectable } from '@angular/core';
import { Transakcija } from '../models/transakcija';
import { KlijentService } from './klijent.service';
import { RacunService } from './racun.service';

@Injectable({
  providedIn: 'root'
})
export class TransakcijaService {

  private transakcije: Transakcija[] = [];

  constructor(private klijentServis: KlijentService, private racunServis: RacunService) {
    this.transakcije.push({
      id: 1,
      tip: "",
      iznos: 5000,
      datumTransakcije: new Date(),
      klijent: klijentServis.getOne(1),
      racun: racunServis.getOne(2)
    })
  }

  getAll() {
    return this.transakcije;
  }

  getOne(id: number) {
    for(let k of this.transakcije) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  // if(this.getOne(klijent.id) == null){
  //   this.klijenti.push(klijent);
  //   return true;
  // }
  // else{
  //   return false;
  // }

  create(transakcija: Transakcija) {
    // if(this.getOne(transakcija.datumTransakcije) > new Date())
    // {
      this.transakcije.push(transakcija);
    // }
    // else{
    //   console.log("Prosao je datum.");
    // }
  }

  update(id: number, transakcija: Transakcija) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije[i] = transakcija;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.transakcije.length; i++) {
      if(this.transakcije[i].id == id) {
        this.transakcije.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
