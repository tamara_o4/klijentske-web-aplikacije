import { Component, Input, OnInit } from '@angular/core';
import { Knjiga } from '../model/knjiga';
import { KnjigaService } from '../service/knjiga.service';

@Component({
  selector: 'app-knjige-izmena',
  templateUrl: './knjige-izmena.component.html',
  styleUrls: ['./knjige-izmena.component.css']
})
export class KnjigeIzmenaComponent implements OnInit {


 
  @Input()
  knjiga: Knjiga = {
    id: 0,
    naslov: "",
    autor: "",
    zanr: ""
  }

  constructor(public knjigaService: KnjigaService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.knjigaService.update(this.knjiga.id, {...this.knjiga});
  }

}
