import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnjigeIzmenaComponent } from './knjige-izmena.component';

describe('KnjigeIzmenaComponent', () => {
  let component: KnjigeIzmenaComponent;
  let fixture: ComponentFixture<KnjigeIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KnjigeIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KnjigeIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
