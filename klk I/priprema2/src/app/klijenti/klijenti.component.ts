import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Klijent } from '../models/klijent';
import { KlijentService } from '../services/klijent.service';

@Component({
  selector: 'app-klijenti',
  templateUrl: './klijenti.component.html',
  styleUrls: ['./klijenti.component.css']
})
export class KlijentiComponent implements OnInit {
  @Output()
  klijentIzmena: EventEmitter<Klijent> = new EventEmitter<Klijent>();

  constructor(public klijentiServis: KlijentService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.klijentiServis.delete(id);
  }

  izmeni(klijent: Klijent){
    this.klijentIzmena.emit({...klijent});
    console.log(klijent);
  }

}
