import { Injectable } from '@angular/core';
import { Kupac } from '../model/kupac';

@Injectable({
  providedIn: 'root'
})
export class KupacService {
  private kupci: Kupac[] = [
    {id: 1, ime: "Sima", prezime: "Simic", email: "dja;jd;a"},
    {id: 2, ime: "Petar", prezime: "Petrovic", email: "dakjdako"},
  ];

  constructor() {
  }

  getAll() {
    return this.kupci;
  }

  getOne(id: number) {
    for(let k of this.kupci) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(transakcija: Kupac) {
      this.kupci.push(transakcija);

  }

  update(id: number, Kupac: Kupac) {
    for(let i = 0; i < this.kupci.length; i++) {
      if(this.kupci[i].id == id) {
        this.kupci[i] = Kupac;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.kupci.length; i++) {
      if(this.kupci[i].id == id) {
        this.kupci.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
