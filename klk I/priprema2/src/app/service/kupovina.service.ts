import { KupacService } from './kupac.service';
import { Injectable } from '@angular/core';
import { ProizvodService } from './proizvod.service';
import { Kupovina } from '../model/kupovina';

@Injectable({
  providedIn: 'root'
})
export class KupovinaService {

  private kupovine: Kupovina[] = [];

  constructor(private kupacService: KupacService, private proizvodService: ProizvodService) {
    this.kupovine.push({
      id: 1,
      datumKupovine: new Date(),
      kolicina:0,
      kupac:kupacService.getOne(1),
      proizvod:proizvodService.getOne(2)
      
    })
  }

  getAll() {
    return this.kupovine;
  }

  getOne(id: number) {
    for(let k of this.kupovine) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  // if(this.getOne(klijent.id) == null){
  //   this.klijenti.push(klijent);
  //   return true;
  // }
  // else{
  //   return false;
  // }

  create(transakcija: Kupovina) {
    // if(this.getOne(transakcija.datumTransakcije) > new Date())
    // {
      this.kupovine.push(transakcija);
    // }
    // else{
    //   console.log("Prosao je datum.");
    // }
  }

  update(id: number, Kupovina: Kupovina) {
    for(let i = 0; i < this.kupovine.length; i++) {
      if(this.kupovine[i].id == id) {
        this.kupovine[i] = Kupovina;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.kupovine.length; i++) {
      if(this.kupovine[i].id == id) {
        this.kupovine.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
