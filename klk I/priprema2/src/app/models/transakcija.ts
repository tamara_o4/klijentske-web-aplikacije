import { Klijent } from "./klijent";
import { Racun } from "./racun";

export interface Transakcija {
    id: number,
    tip: string,
    iznos: number,
    datumTransakcije: Date,
    klijent: Klijent|null,
    racun: Racun|null
}