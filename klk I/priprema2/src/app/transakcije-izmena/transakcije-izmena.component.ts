import { Component, Input, OnInit } from '@angular/core';
import { Transakcija } from '../models/transakcija';
import { KlijentService } from '../services/klijent.service';
import { RacunService } from '../services/racun.service';
import { TransakcijaService } from '../services/transakcija.service';

@Component({
  selector: 'app-transakcije-izmena',
  templateUrl: './transakcije-izmena.component.html',
  styleUrls: ['./transakcije-izmena.component.css']
})
export class TransakcijeIzmenaComponent implements OnInit {

  tipoviTransakcije: string[] = ["Uplata", "Isplata"];
  @Input()
  transakcija: Transakcija = {
    id: 1,
    iznos: 0,
    tip: "Uplata",
    datumTransakcije: new Date(),
    klijent: null,
    racun: null
  };
  constructor(public klijentServis: KlijentService, public racunServis: RacunService, private transkacijaServis: TransakcijaService) { }

  ngOnInit(): void {
  }

  izmeni() {
    this.transkacijaServis.update(this.transakcija.id, {...this.transakcija});
  }

}
