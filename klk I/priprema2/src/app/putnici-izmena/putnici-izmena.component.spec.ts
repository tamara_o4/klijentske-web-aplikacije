import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PutniciIzmenaComponent } from './putnici-izmena.component';

describe('PutniciIzmenaComponent', () => {
  let component: PutniciIzmenaComponent;
  let fixture: ComponentFixture<PutniciIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PutniciIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PutniciIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
