import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PutniciDodavanjeComponent } from './putnici-dodavanje.component';

describe('PutniciDodavanjeComponent', () => {
  let component: PutniciDodavanjeComponent;
  let fixture: ComponentFixture<PutniciDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PutniciDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PutniciDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
