import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { KartaService } from '../services/karta.service';
import { Karta } from '../models/karta';


@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.css']
})
export class KarteComponent implements OnInit {
  @Output()
  kartaIzmena: EventEmitter<Karta> = new EventEmitter<Karta>();
  constructor(public kartaService:KartaService) { }

  ngOnInit(): void {
  }
  ukloni(id: number) {
    this.kartaService.delete(id);
  }

  izmeni(karta: Karta) {
    this.kartaIzmena.emit({...karta});
  }


}
