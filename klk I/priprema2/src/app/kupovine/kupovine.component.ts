import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Kupovina } from '../model/kupovina';
import { KupovinaService } from '../service/kupovina.service';

@Component({
  selector: 'app-kupovine',
  templateUrl: './kupovine.component.html',
  styleUrls: ['./kupovine.component.css']
})
export class KupovineComponent implements OnInit {
  @Output()
  kupovinaIzmena: EventEmitter<Kupovina> = new EventEmitter<Kupovina>();
  constructor(public kupovinaService:KupovinaService) { }

  ngOnInit(): void {
  }
  ukloni(id: number) {
    this.kupovinaService.delete(id);
  }

  izmeni(kupovina: Kupovina) {
    this.kupovinaIzmena.emit({...kupovina});
  }

}
