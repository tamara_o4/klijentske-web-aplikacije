import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Kupac } from '../model/kupac';
import { KupacService } from '../service/kupac.service';

@Component({
  selector: 'app-kupci',
  templateUrl: './kupci.component.html',
  styleUrls: ['./kupci.component.css']
})
export class KupciComponent implements OnInit {
  @Output()
  kupacIzmena: EventEmitter<Kupac> = new EventEmitter<Kupac>();
 
  

  constructor(public kupacService: KupacService) { }

  ngOnInit(): void {
  }

  ukloni(id: number){
    this.kupacService.delete(id);
  }

  izmeni(kupac: Kupac){
    this.kupacIzmena.emit({...kupac});
    console.log(kupac);
  }

}
