import { Proizvod } from './model/proizvod';
import { Kupovina } from './model/kupovina';
import { Kupac } from './model/kupac';
import { Component } from '@angular/core';
import { Karta } from './models/karta';
import { Klijent } from './models/klijent';
import { Putnik } from './models/putnik';
import { Racun } from './models/racun';
import { Stanica } from './models/stanica';
import { Transakcija } from './models/transakcija';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'priprema';
  
  klijentZaIzmenu: Klijent|null = null;
  izmeni(klijent: Klijent){
    this.klijentZaIzmenu = klijent;
  }

  racunZaIzmenu: Racun|null = null;
  izmeni2(racun: Racun){
    this.racunZaIzmenu = racun;
  }

  transakcijaZaIzmenu: Transakcija|null = null;

  izmeni3(transakcija: Transakcija) {
    this.transakcijaZaIzmenu = transakcija;
  }

  kartaIzmena: Karta|null = null;

  izmeni4(karta: Karta) {
    this.kartaIzmena = karta;
  }
  putnikIzmena: Putnik|null = null;

  izmeni5(putnik: Putnik) {
    this.putnikIzmena = putnik;
  }
  stanicaIzmena: Stanica|null = null;
  izmeni6(stanica: Stanica) {
    this.stanicaIzmena = stanica;
  }

  kupacIzmena: Kupac|null = null;
  izmeni7(kupac: Kupac) {
    this.kupacIzmena = kupac;
  }

  kupovinaIzmena: Kupovina|null = null;
  izmeni8(kupovina: Kupovina) {
    this.kupovinaIzmena = kupovina;
  }

  proizvodIzmena: Proizvod|null = null;
  izmeni9(proizvod: Proizvod) {
    this.proizvodIzmena = proizvod;
  }

}
