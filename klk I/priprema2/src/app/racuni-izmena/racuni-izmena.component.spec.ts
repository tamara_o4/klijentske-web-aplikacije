import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacuniIzmenaComponent } from './racuni-izmena.component';

describe('RacuniIzmenaComponent', () => {
  let component: RacuniIzmenaComponent;
  let fixture: ComponentFixture<RacuniIzmenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacuniIzmenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacuniIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
