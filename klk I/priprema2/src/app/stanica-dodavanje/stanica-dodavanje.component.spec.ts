import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StanicaDodavanjeComponent } from './stanica-dodavanje.component';

describe('StanicaDodavanjeComponent', () => {
  let component: StanicaDodavanjeComponent;
  let fixture: ComponentFixture<StanicaDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StanicaDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StanicaDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
