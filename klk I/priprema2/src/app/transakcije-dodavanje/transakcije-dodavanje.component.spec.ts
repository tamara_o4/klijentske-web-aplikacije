import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransakcijeDodavanjeComponent } from './transakcije-dodavanje.component';

describe('TransakcijeDodavanjeComponent', () => {
  let component: TransakcijeDodavanjeComponent;
  let fixture: ComponentFixture<TransakcijeDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransakcijeDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransakcijeDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
