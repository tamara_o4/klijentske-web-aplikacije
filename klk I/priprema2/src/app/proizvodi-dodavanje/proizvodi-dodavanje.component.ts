import { Component, OnInit } from '@angular/core';
import { Proizvod } from '../model/proizvod';
import { ProizvodService } from '../service/proizvod.service';

@Component({
  selector: 'app-proizvodi-dodavanje',
  templateUrl: './proizvodi-dodavanje.component.html',
  styleUrls: ['./proizvodi-dodavanje.component.css']
})
export class ProizvodiDodavanjeComponent implements OnInit {

  proizvod: Proizvod = {
    id: 0,
    naziv: "",
    opis: "",
    cena: 0
  }

  constructor(public proizvodService: ProizvodService) { }

  ngOnInit(): void {
  }

  dodaj(){
    if(this.proizvodService.create({...this.proizvod}) == true)
    {
      this.proizvodService.create({...this.proizvod});
    }
    else{
      console.log("Korisnik sa tim ID postoji.");
    }
  }

}
