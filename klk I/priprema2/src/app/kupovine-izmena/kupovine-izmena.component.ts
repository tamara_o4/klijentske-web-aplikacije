import { ProizvodService } from './../service/proizvod.service';
import { KupacService } from './../service/kupac.service';
import { Component, Input, OnInit } from '@angular/core';
import { Kupovina } from '../model/kupovina';
import { KupovinaService } from '../service/kupovina.service';

@Component({
  selector: 'app-kupovine-izmena',
  templateUrl: './kupovine-izmena.component.html',
  styleUrls: ['./kupovine-izmena.component.css']
})
export class KupovineIzmenaComponent implements OnInit {

  tiprtljaga: string[] = ["True", "False"];
  @Input()
  kupovina: Kupovina = {
    id: 1,
    datumKupovine: new Date(),
    kolicina:0,
    kupac: null,
    proizvod: null
  };


  constructor(public kupacService: KupacService, public proizvodService: ProizvodService, private kupovinaService: KupovinaService) { }

  ngOnInit(): void {
  }
  izmeni() {
    this.kupovinaService.update(this.kupovina.id, {...this.kupovina});
  }
}
