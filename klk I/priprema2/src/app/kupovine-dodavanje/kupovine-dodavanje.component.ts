import { ProizvodService } from './../service/proizvod.service';
import { KupacService } from './../service/kupac.service';
import { Component, OnInit } from '@angular/core';
import { Kupovina } from '../model/kupovina';
import { KupovinaService } from '../service/kupovina.service';

@Component({
  selector: 'app-kupovine-dodavanje',
  templateUrl: './kupovine-dodavanje.component.html',
  styleUrls: ['./kupovine-dodavanje.component.css']
})
export class KupovineDodavanjeComponent implements OnInit {

  kupovina: Kupovina = {
    id: 1,
    datumKupovine: new Date(),
    kolicina:0,
  
    kupac: null,
    proizvod: null
  };

  constructor(public kupacService: KupacService, public proizvodService: ProizvodService, private kupovinaService: KupovinaService) { }

  ngOnInit(): void {
  }

  dodaj() {
    this.kupovinaService.create({...this.kupovina});
    console.log(this.kupovina);
  }

}
