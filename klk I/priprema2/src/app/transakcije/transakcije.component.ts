import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Transakcija } from '../models/transakcija';
import { TransakcijaService } from '../services/transakcija.service';

@Component({
  selector: 'app-transakcije',
  templateUrl: './transakcije.component.html',
  styleUrls: ['./transakcije.component.css']
})
export class TransakcijeComponent implements OnInit {

  @Output()
  transakcijaIzmena: EventEmitter<Transakcija> = new EventEmitter<Transakcija>();

  constructor(public transakcijeServis: TransakcijaService) { }

  ngOnInit(): void {
  }

  ukloni(id: number) {
    this.transakcijeServis.delete(id);
  }

  izmeni(transakcija: Transakcija) {
    this.transakcijaIzmena.emit({...transakcija});
  }

}
