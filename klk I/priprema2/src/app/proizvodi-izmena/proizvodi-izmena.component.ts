import { Component, Input, OnInit } from '@angular/core';
import { Proizvod } from '../model/proizvod';
import { ProizvodService } from '../service/proizvod.service';

@Component({
  selector: 'app-proizvodi-izmena',
  templateUrl: './proizvodi-izmena.component.html',
  styleUrls: ['./proizvodi-izmena.component.css']
})
export class ProizvodiIzmenaComponent implements OnInit {
  @Input()
  proizvod: Proizvod = {
    id: 0,
    naziv: "",
    opis: "",
    cena: 0
  }

  constructor(public proizvodService: ProizvodService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.proizvodService.update(this.proizvod.id, {...this.proizvod});
  }


}
