import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KarteDodavanjeComponent } from './karte-dodavanje.component';

describe('KarteDodavanjeComponent', () => {
  let component: KarteDodavanjeComponent;
  let fixture: ComponentFixture<KarteDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KarteDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KarteDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
