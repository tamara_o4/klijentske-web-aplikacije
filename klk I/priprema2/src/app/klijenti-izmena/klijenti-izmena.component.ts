import { Component, Input, OnInit } from '@angular/core';
import { Klijent } from '../models/klijent';
import { KlijentService } from '../services/klijent.service';

@Component({
  selector: 'app-klijenti-izmena',
  templateUrl: './klijenti-izmena.component.html',
  styleUrls: ['./klijenti-izmena.component.css']
})
export class KlijentiIzmenaComponent implements OnInit {

  @Input()
  klijent: Klijent = {
    id: 0,
    ime: "",
    prezime: ""
  }

  constructor(public klijentiServis: KlijentService) { }

  ngOnInit(): void {
  }

  izmeni(){
    this.klijentiServis.update(this.klijent.id, {...this.klijent});
  }

}
