import { Component, OnInit } from '@angular/core';
import { Racun } from '../models/racun';
import { RacunService } from '../services/racun.service';

@Component({
  selector: 'app-racuni-dodavanje',
  templateUrl: './racuni-dodavanje.component.html',
  styleUrls: ['./racuni-dodavanje.component.css']
})
export class RacuniDodavanjeComponent implements OnInit {

  racun: Racun = {
    id: 0,
    brojRacuna: "",
    stanje: 0
  }

  constructor(public racuniServis: RacunService) { }

  ngOnInit(): void {
  }

  dodaj(){
    this.racuniServis.create({...this.racun});
  }

}
