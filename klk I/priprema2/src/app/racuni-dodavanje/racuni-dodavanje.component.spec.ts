import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacuniDodavanjeComponent } from './racuni-dodavanje.component';

describe('RacuniDodavanjeComponent', () => {
  let component: RacuniDodavanjeComponent;
  let fixture: ComponentFixture<RacuniDodavanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacuniDodavanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacuniDodavanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
