import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SmeroviComponent } from './smerovi/smerovi.component';
import { DodajSmerComponent } from './smerovi/dodaj-smer/dodaj-smer.component';

@NgModule({
  declarations: [
    AppComponent,
    SmeroviComponent,
    DodajSmerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
     FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
