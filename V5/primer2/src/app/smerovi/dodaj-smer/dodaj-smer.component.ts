import { Component, EventEmitter, OnInit, Output } from '@angular/core';
@Component({
  selector: 'app-dodaj-smer',
  templateUrl: './dodaj-smer.component.html',
  styleUrls: ['./dodaj-smer.component.css']
})
export class DodajSmerComponent implements OnInit {
  smer = {
    "sifra": "",
    "naziv": ""
  }

  @Output()
  dodavanjeSmera:EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }
  dodaj() {
    this.dodavanjeSmera.emit({...this.smer});
  }

}
