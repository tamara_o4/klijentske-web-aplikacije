import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Prikaz studenata';
  noviStudent = 
    { indeks: "", ime: "", prezime: "", smer: "", prosecnaOcena: 0 }
  studenti = [
    { indeks: "1234/123456", ime: "Ime1", prezime: "Prezime1", smer: "Smer1", prosecnaOcena: 8.2 },
    { indeks: "2234/123456", ime: "Ime2", prezime: "Prezime2", smer: "Smer1", prosecnaOcena: 10 },
  ]

  dodajStudenta() {
    this.studenti.push({...this.noviStudent});
  }
  
  ukloniStudenta(index:number) {
    this.studenti.splice(index, 1);
  }
}
