import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Ticket } from '../model/ticket';
import { LoginService } from '../services/login.service';
import { TicketService } from '../services/ticket.service';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  tickets: Ticket[] = [];
  ticketUpdate: Ticket|null = null;
  
  constructor(private ticketService: TicketService, private router: Router,public loginService: LoginService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.ticketService.getAll().subscribe((value)=>{
      this.tickets = value;
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id:number) {

    this.ticketService.delete(id).subscribe(r=>{

      this.ticketService.getAll().subscribe(r=>{

        this.tickets = r;

      })

    });
  }

  create(ticket: Ticket){
    this.ticketService.create(ticket).subscribe((value)=>{
      this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  update(ticket: Ticket){
    if(this.ticketUpdate && this.ticketUpdate.id){
      this.ticketService.update(this.ticketUpdate.id, ticket).subscribe((value)=>{
        this.getAll();
      }, (error)=>{
        console.log(error);
      })
    }
  }

  setUpdate(user:any){
    this.ticketUpdate = {...user};
  }

}
