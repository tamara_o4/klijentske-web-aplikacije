import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { TicketsComponent } from './tickets/tickets.component';

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "tickets", component: TicketsComponent, canActivate: [AuthGuard], data: {allowedRoles: ["ROLE_ADMIN", "ROLE_USER"]}},
  {path: "purchases", component: PurchaseComponent, canActivate: [AuthGuard], data: {allowedRoles: ["ROLE_ADMIN", "ROLE_USER"]}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
