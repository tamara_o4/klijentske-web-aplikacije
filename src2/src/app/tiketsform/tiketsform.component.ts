import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Ticket } from '../model/ticket';

@Component({
  selector: 'app-tiketsform',
  templateUrl: './tiketsform.component.html',
  styleUrls: ['./tiketsform.component.css']
})
export class TiketsformComponent implements OnInit {

  @Input()
  ticket: Ticket|null = null;

  @Output()
  public createEvent: EventEmitter<any> = new EventEmitter<any>();

  forma: FormGroup = new FormGroup({
    id: new FormControl(),
    from: new FormControl(),
    to: new FormControl(),
    return: new FormControl(),
    price: new FormControl()
  });

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.forma.get("id")?.setValue(this.ticket?.id);
    this.forma.get("from")?.setValue(this.ticket?.from);
    this.forma.get("to")?.setValue(this.ticket?.to);
    this.forma.get("return")?.setValue(this.ticket?.return);
    this.forma.get("price")?.setValue(this.ticket?.price);
  }

  ngOnInit(): void {
    this.forma.get("id")?.setValue(this.ticket?.id);
    this.forma.get("from")?.setValue(this.ticket?.from);
    this.forma.get("to")?.setValue(this.ticket?.to);
    this.forma.get("return")?.setValue(this.ticket?.return);
    this.forma.get("price")?.setValue(this.ticket?.price);
  }
  
  create(){
    if(this.forma.valid){
      this.createEvent.emit(this.forma.value);
    }
  }
}
