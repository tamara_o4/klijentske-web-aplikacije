import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Purchase } from '../model/purchase';
import { Ticket } from '../model/ticket';
import { PurchaseService } from '../services/purchase.service';

@Component({
  selector: 'app-purchaseform',
  templateUrl: './purchaseform.component.html',
  styleUrls: ['./purchaseform.component.css']
})
export class PurchaseformComponent implements OnInit {

 
  @Input()
  ticketId: Ticket[] = [];

  @Input()
  purchase: Purchase|null = null;

  @Output()
  private createEvent: EventEmitter<any> = new EventEmitter<any>();

  forma: FormGroup = new FormGroup({
    id: new FormControl(),
    ticketId: new FormControl(),
    purchaseDate: new FormControl(),
    used: new FormControl()
  });

  constructor(private purchaseService: PurchaseService, private router: Router){
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.forma.get("id")?.setValue(this.purchase?.id);
    this.forma.get("ticketId")?.setValue(this.purchase?.ticketId);
    this.forma.get("purchaseDate")?.setValue(this.purchase?.purchaseDate);
    this.forma.get("used")?.setValue(this.purchase?.used);
  }

  ngOnInit(): void {
    this.forma.get("id")?.setValue(this.purchase?.id);
    this.forma.get("ticketId")?.setValue(this.purchase?.ticketId);
    this.forma.get("purchaseDate")?.setValue(this.purchase?.purchaseDate);
    this.forma.get("used")?.setValue(this.purchase?.used);
  }

 create() {
   if(this.forma.valid) {
     console.log(this.forma.value)
     this.createEvent.emit(this.forma.value); 
     
   }
 }
}
