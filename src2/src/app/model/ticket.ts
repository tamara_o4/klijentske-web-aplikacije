export interface Ticket {
    id?: number,
    from: string,
    to: string,
    return: boolean,
    price: number
}
