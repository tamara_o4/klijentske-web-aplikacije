const GenerickaTabela = {
    props: ["zaglavlje", "sadrzaj"],
    emits: ["izmeni", "ukloni"],
    data() {
        return {
        }
    },
    template: `
    <table>
        <thead>
            <tr>
                <th v-for="naziv in zaglavlje">{{naziv}}</th>
                <th>Akcije</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(red, index) in sadrzaj">
                <td v-for="(naziv, kljuc) in zaglavlje">{{red[kljuc]}}</td>
                <td>
                    <button v-on:click="$emit('izmeni', {index, podaci: red})">Izmeni</button>
                    <button v-on:click="$emit('ukloni', {index, podaci: red})">Ukloni</button>
                </td>
            </tr>
        </tbody>
    </table>
    `
}