import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiketsformComponent } from './tiketsform.component';

describe('TiketsformComponent', () => {
  let component: TiketsformComponent;
  let fixture: ComponentFixture<TiketsformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TiketsformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TiketsformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
