import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ticket } from '../model/ticket';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Ticket[]>("http://localhost:3000/tickets");
  }

  getOne(id: number) {
    return this.httpClient.get<Ticket>(`http://localhost:3000/tickets/${id}`);
  }

  create(ticket: Ticket) {
    return this.httpClient.post<Ticket>("http://localhost:3000/tickets", ticket);
  }

  update(id: number, ticket: Ticket) {
    return this.httpClient.put<Ticket>(`http://localhost:3000/tickets/${id}`, ticket);
  }

  delete(id: number) {
    return this.httpClient.delete<Ticket>(`http://localhost:3000/tickets/${id}`);
  }
}
