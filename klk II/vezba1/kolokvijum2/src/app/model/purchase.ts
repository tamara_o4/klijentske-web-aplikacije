import { Ticket } from "./ticket";

export interface Purchase {
    id?: number,
    ticketId: Ticket,
    purchaseDate: string,
    used: boolean
}
