import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../model/books';
import { BooksService } from '../services/books.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  bookss: Books[] = [];
  booksUpdate: Books|null = null;
  
  constructor(private booksService: BooksService, private router: Router,public loginService: LoginService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.booksService.getAll().subscribe((value)=>{
      this.bookss = value;
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id:number) {

    this.booksService.delete(id).subscribe(r=>{

      this.booksService.getAll().subscribe(r=>{

        this.bookss = r;

      })

    });
  }

  create(books: Books){
    this.booksService.create(books).subscribe((value)=>{
      this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  update(books: Books){
    if(this.booksUpdate && this.booksUpdate.id){
      this.booksService.update(this.booksUpdate.id, books).subscribe((value)=>{
        this.getAll();
      }, (error)=>{
        console.log(error);
      })
    }
  }

  setUpdate(user:any){
    this.booksUpdate = {...user};
  }

}
