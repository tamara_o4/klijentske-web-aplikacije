import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Purchase } from '../model/purchase';
import { Ticket } from '../model/ticket';
import { LoginService } from '../services/login.service';
import { PurchaseService } from '../services/purchase.service';
import { TicketService } from '../services/ticket.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  purchases: Purchase[] = [];
  purchaseUpdate: Purchase|null = null;

  ticketId: Ticket[];
  
  constructor(private purchaseService: PurchaseService, private router: Router,
    private ticketService: TicketService, public loginService: LoginService) { }

  ngOnInit(): void {
    this.getAll();
    this.getAllTickets();
  }

  getAll(){
    this.purchaseService.getAll().subscribe((value)=>{
      this.purchases = value;
      console.log(value);
    }, (error)=>{
      console.log(error);
    });
  }

  getAllTickets(){
    this.ticketService.getAll().subscribe((value)=>{
      this.ticketId = value;
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id:any){
    this.purchaseService.delete(id).subscribe((value)=>{
    this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  create(purchase: Purchase){
    this.purchaseService.create(purchase).subscribe((value)=>{
      this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  update(purchase: Purchase){
    if(this.purchaseUpdate && this.purchaseUpdate.id){
      this.purchaseService.update(this.purchaseUpdate.id, purchase).subscribe((value)=>{
        this.getAll();
      }, (error)=>{
        console.log(error);
      })
    }
  }

  setUpdate(purchase:any){
    this.purchaseUpdate = {...purchase};
  }
}
