import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';

import { PurchaseComponent } from './purchase/purchase.component';
import { TicketsComponent } from './tickets/tickets.component';
import { LoginComponent } from './login/login.component';

import { TiketsformComponent } from './tiketsform/tiketsform.component';
import { PurchaseformComponent } from './purchaseform/purchaseform.component';
import { BooksComponent } from './books/books.component';
import { BooksFormComponent } from './books-form/books-form.component';
import { RentsComponent } from './rents/rents.component';
import { RentsFormComponent } from './rents-form/rents-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PurchaseComponent,
    TicketsComponent,

    TiketsformComponent,
    PurchaseformComponent,
    BooksComponent,
    BooksFormComponent,
    RentsComponent,
    RentsFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
