import { FormControl, FormGroup } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Books } from '../model/books';
import { Rents } from '../model/rents';
import { Router } from '@angular/router';
import { RentsService } from '../services/rents.service';

@Component({
  selector: 'app-rents-form',
  templateUrl: './rents-form.component.html',
  styleUrls: ['./rents-form.component.css']
})
export class RentsFormComponent implements OnInit {

  @Input()
  booksId: Books[] = [];

  @Input()
  rents: Rents|null = null;

  @Output()
  private createEvent: EventEmitter<any> = new EventEmitter<any>();

  forma: FormGroup = new FormGroup({
    id: new FormControl(),
    booksId: new FormControl(),
    rentDate: new FormControl(),
    returnDate: new FormControl(),
    state: new FormControl(),
    price: new FormControl()
  });

  constructor(private rentsService: RentsService, private router: Router){
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.forma.get("id")?.setValue(this.rents?.id);
    this.forma.get("booksId")?.setValue(this.rents?.books);
    this.forma.get("rentDate")?.setValue(this.rents?.rentDate);
    this.forma.get("state")?.setValue(this.rents?.state);
    this.forma.get("price")?.setValue(this.rents?.price);
  }

  ngOnInit(): void {
    this.forma.get("id")?.setValue(this.rents?.id);
    this.forma.get("booksId")?.setValue(this.rents?.books);
    this.forma.get("rentDate")?.setValue(this.rents?.rentDate);
    this.forma.get("state")?.setValue(this.rents?.state);
    this.forma.get("price")?.setValue(this.rents?.price);
  }

 create() {
   if(this.forma.valid) {
     console.log(this.forma.value)
     this.createEvent.emit(this.forma.value); 
     
   }
 }

}
