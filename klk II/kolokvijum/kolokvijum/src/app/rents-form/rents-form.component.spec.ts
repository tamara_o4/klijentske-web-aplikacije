import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentsFormComponent } from './rents-form.component';

describe('RentsFormComponent', () => {
  let component: RentsFormComponent;
  let fixture: ComponentFixture<RentsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
