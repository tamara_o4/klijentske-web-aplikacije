import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { create } from 'domain';
import { Books } from '../model/books';

@Component({
  selector: 'app-books-form',
  templateUrl: './books-form.component.html',
  styleUrls: ['./books-form.component.css']
})
export class BooksFormComponent implements OnInit {
  @Input()
  books: Books|null = null;

  @Output()
  public createEvent: EventEmitter<any> = new EventEmitter<any>();

  forma: FormGroup = new FormGroup({
    id: new FormControl(),
    title: new FormControl(),
    author: new FormControl(),
    genre: new FormControl()
  });

  construcauthorr() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.forma.get("id")?.setValue(this.books?.id);
    this.forma.get("title")?.setValue(this.books?.title);
    this.forma.get("author")?.setValue(this.books?.author);
    this.forma.get("genre")?.setValue(this.books?.genre);
  }

  ngOnInit(): void {
    this.forma.get("id")?.setValue(this.books?.id);
    this.forma.get("title")?.setValue(this.books?.title);
    this.forma.get("author")?.setValue(this.books?.author);
    this.forma.get("genre")?.setValue(this.books?.genre);
  }
  
  create(){
    if(this.forma.valid){
      this.createEvent.emit(this.forma.value);
    }
  }

}
