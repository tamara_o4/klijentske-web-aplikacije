import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Books } from '../model/books';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Books[]>("http://localhost:3000/books");
  }

  getOne(id: number) {
    return this.httpClient.get<Books>(`http://localhost:3000/books/${id}`);
  }

  create(ticket: Books) {
    return this.httpClient.post<Books>("http://localhost:3000/books", ticket);
  }

  update(id: number, ticket: Books) {
    return this.httpClient.put<Books>(`http://localhost:3000/books/${id}`, ticket);
  }

  delete(id: number) {
    return this.httpClient.delete<Books>(`http://localhost:3000/books/${id}`);
  }
}
