import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Rents } from '../model/purchase';

@Injectable({
  providedIn: 'root'
})
export class RentsService {
  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Rents[]>("http://localhost:3000/rents");
  }

  getOne(id: number) {
    return this.httpClient.get<Rents>(`http://localhost:3000/rents/${id}`);
  }

  create(purchase: Rents) {
    return this.httpClient.post<Rents>("http://localhost:3000/rents", purchase);
  }

  update(id: number, purchase: Rents) {
    return this.httpClient.put<Rents>(`http://localhost:3000/rents/${id}`, purchase);
  }

  delete(id: number) {
    return this.httpClient.delete<Rents>(`http://localhost:3000/rents/${id}`);
  }
}
