import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Purchase } from '../model/purchase';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Purchase[]>("http://localhost:3000/purchases");
  }

  getOne(id: number) {
    return this.httpClient.get<Purchase>(`http://localhost:3000/purchases/${id}`);
  }

  create(purchase: Purchase) {
    return this.httpClient.post<Purchase>("http://localhost:3000/purchases", purchase);
  }

  update(id: number, purchase: Purchase) {
    return this.httpClient.put<Purchase>(`http://localhost:3000/purchases/${id}`, purchase);
  }

  delete(id: number) {
    return this.httpClient.delete<Purchase>(`http://localhost:3000/purchases/${id}`);
  }
}
