import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../model/books';
import { Rents } from '../model/rents';
import { BooksService } from '../services/books.service';
import { LoginService } from '../services/login.service';
import { RentsService } from '../services/rents.service';

@Component({
  selector: 'app-rents',
  templateUrl: './rents.component.html',
  styleUrls: ['./rents.component.css']
})
export class RentsComponent implements OnInit {

  rentss: Rents[] = [];
  rentsUpdate: Rents|null = null;

  booksId: Books[];
  
  constructor(private rentsService: RentsService, private router: Router,
    private ticketService: BooksService, public loginService: LoginService) { }

  ngOnInit(): void {
    this.getAll();
    this.getAllBookss();
  }

  getAll(){
    this.rentsService.getAll().subscribe((value)=>{
      this.rentss = value;
      console.log(value);
    }, (error)=>{
      console.log(error);
    });
  }

  getAllBookss(){
    this.ticketService.getAll().subscribe((value)=>{
      this.booksId = value;
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id:any){
    this.rentsService.delete(id).subscribe((value)=>{
    this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  create(rents: Rents){
    this.rentsService.create(rents).subscribe((value)=>{
      this.getAll();
    }, (error)=>{
      console.log(error);
    })
  }

  update(rents: Rents){
    if(this.rentsUpdate && this.rentsUpdate.id){
      this.rentsService.update(this.rentsUpdate.id, rents).subscribe((value)=>{
        this.getAll();
      }, (error)=>{
        console.log(error);
      })
    }
  }

  setUpdate(rents:any){
    this.rentsUpdate = {...rents};
  }

}
