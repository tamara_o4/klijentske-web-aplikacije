import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { RentsComponent } from './rents/rents.component';

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "books", component: BooksComponent, canActivate: [AuthGuard], data: {allowedRoles: ["ROLE_ADMIN", "ROLE_USER"]}},
  {path: "rents", component: RentsComponent, canActivate: [AuthGuard], data: {allowedRoles: ["ROLE_ADMIN", "ROLE_USER"]}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
