import { Books } from './books';
export interface Rents {
    id?: number,
    books: Books,
    rentDate: Date,
    returnDate: Date,
    state: string,
    price: number
}
